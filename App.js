import { createAppContainer } from 'react-navigation'

import MainNavigator from './src/components/navigator/MainNavigator/MainNavigator'

export default createAppContainer(MainNavigator)
