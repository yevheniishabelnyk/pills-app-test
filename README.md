## Installing

1. Make `git clone https://yevheniishabelnyk@bitbucket.org/yevheniishabelnyk/pills-app-test.git` in your terminal;
2. Go to the path with the project;
3. In the root path make `yarn` or `npm install` in terminal, for install all dependecies;
4. `expo start`.

## P.S.

Main navigations are in the `src/components/navigator/MainNavigator/MainNavigator.js` file. Yes, this file some long, but I decided not to separate it for small components, for more understanding :)

Thanks!
