import React from 'react'
import PropTypes from 'prop-types'

import { View, Text } from 'react-native'

import styles from './styles'

const Messages = ({ title, messages }) => {
  return (
    <View style={styles.messagesWrapper}>
      {title && <Text style={styles.messagesTitle}>{title}</Text>}

      {messages && !!messages.length ? (
        messages.map((message, index) => <Text key={index}>{messages}</Text>)
      ) : (
        <Text>No symptoms today!</Text>
      )}
    </View>
  )
}

Messages.propTypes = {
  title: PropTypes.string.isRequired,
  messages: PropTypes.array,
}

Messages.defaultProps = {
  title: 'Messages',
}

export default Messages
