import { moderateScale } from 'src/utils/scaling'

export default {
  messagesWrapper: {
    marginTop: moderateScale(40),
  },

  messagesTitle: {
    fontSize: moderateScale(30),
    marginBottom: moderateScale(20),
  },
}
