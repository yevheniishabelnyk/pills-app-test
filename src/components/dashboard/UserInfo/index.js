import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, Image } from 'react-native'

import defaultAvatarIcon from 'src/assets/no-avatar-icon.png'

import styles from './styles'

const UserInfo = ({ avatar, text }) => {
  return (
    <View style={styles.userInfo}>
      {avatar ? (
        <Image source={avatar} style={styles.avatar} />
      ) : (
        <Image source={defaultAvatarIcon} style={styles.avatar} />
      )}

      {text && <Text style={styles.welcomeText}>{text}</Text>}
    </View>
  )
}

UserInfo.propTypes = {
  avatar: PropTypes.number,
  text: PropTypes.string,
}

UserInfo.defaultProps = {
  text: 'No info',
}

export default UserInfo
