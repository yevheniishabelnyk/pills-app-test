import { moderateScale } from 'src/utils/scaling'

export default {
  userInfo: {
    flexDirection: 'row',
    alignItems: 'center',
  },

  avatar: {
    width: moderateScale(150),
    height: moderateScale(150),
  },

  welcomeText: {
    fontSize: moderateScale(28),
  },
}
