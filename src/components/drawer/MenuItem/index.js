import React from 'react'
import PropTypes from 'prop-types'

import { Text, View } from 'react-native'

import styles from './styles'

const MenuItem = ({ title, onPress, style, currentRoute }) => {
  return (
    <View style={[styles.navItem, style]}>
      {title && (
        <Text
          style={[
            styles.navItemText,
            currentRoute ? styles.activeNavItem : null,
          ]}
          onPress={onPress}
        >
          {title}
        </Text>
      )}
    </View>
  )
}

MenuItem.propTypes = {
  title: PropTypes.string.isRequired,
  onPress: PropTypes.func,
  style: PropTypes.object,
  currentRoute: PropTypes.bool,
}

export default MenuItem
