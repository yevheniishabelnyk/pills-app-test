import { moderateScale } from 'src/utils/scaling'

export default {
  navItem: {
    borderBottomWidth: 1,
  },

  navItemText: {
    fontSize: moderateScale(16),
    padding: moderateScale(10),
  },

  activeNavItem: {
    color: '#3399ff',
    fontWeight: 'bold',
  },
}
