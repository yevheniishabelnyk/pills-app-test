import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { View } from 'react-native'

import { NavigationActions, DrawerActions } from 'react-navigation'

import MenuItem from 'src/components/drawer/MenuItem'

import styles from './styles'

const navs = [
  { id: 1, title: 'DashBoard', link: 'DashBoard' },
  { id: 2, title: 'Medication', link: 'Medication' },
  { id: 3, title: 'Calendar', link: 'Calendar' },
]

class SideMenu extends Component {
  constructor(props) {
    super(props)

    this.state = {
      routeName: props.initialRoute,
    }
  }

  navigateToScreen = route => {
    const navigateAction = NavigationActions.navigate({
      routeName: route,
    })

    this.props.navigation.dispatch(navigateAction)
    this.props.navigation.dispatch(DrawerActions.closeDrawer())
  }

  componentDidUpdate(prevProps) {
    if (
      prevProps.navigation &&
      prevProps.navigation !== this.props.navigation
    ) {
      const { state } = this.props.navigation
      const index = this.props.navigation.state.index
      const routeIndex = state.routes[index].routes[index].index

      const route =
        state.routes[index].routes[index].routes[routeIndex].routeName

      this.setState({ routeName: route })
    }
  }

  render() {
    const { routeName } = this.state

    return (
      <View style={styles.container}>
        {!!navs.length &&
          navs.map((nav, index) => (
            <MenuItem
              key={index}
              style={index === 0 ? styles.borderTop : null}
              title={nav.title}
              onPress={() => this.navigateToScreen(nav.link)}
              currentRoute={routeName === nav.link}
            />
          ))}
      </View>
    )
  }
}

SideMenu.propTypes = {
  navigation: PropTypes.object,
  initialRoute: PropTypes.string,
}

export default SideMenu
