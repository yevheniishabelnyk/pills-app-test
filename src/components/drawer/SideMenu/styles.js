import { moderateScale } from 'src/utils/scaling'

export default {
  container: {
    paddingTop: moderateScale(50),
    flex: 1,
  },

  borderTop: {
    borderTopWidth: 1,
  },
}
