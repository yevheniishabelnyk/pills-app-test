import React from 'react'
import PropTypes from 'prop-types'

import { View, Text, TouchableOpacity, Image } from 'react-native'

import checkedIcon from 'src/assets/checked-icon.png'

import styles from './styles'

const Checkbox = ({ name, time, checked, onPress }) => {
  const onButtonPressed = (name, time) => {
    if (typeof onPress == 'function') {
      onPress(name, time)
    }
  }

  return (
    <TouchableOpacity onPress={() => onButtonPressed(name, time)}>
      <View style={styles.wrapper}>
        {time ? (
          <Text style={styles.time}>{time}</Text>
        ) : (
          <Text style={styles.time}>-:-</Text>
        )}

        {name ? (
          <Text style={styles.pillName}>{name}</Text>
        ) : (
          <Text style={styles.pillName}>Unknown</Text>
        )}

        <View style={styles.square}>
          {checked ? <Image source={checkedIcon} style={styles.icon} /> : null}
        </View>
      </View>
    </TouchableOpacity>
  )
}

Checkbox.propTypes = {
  name: PropTypes.string.isRequired,
  time: PropTypes.string.isRequired,
  checked: PropTypes.bool.isRequired,
  onPress: PropTypes.func,
}

export default Checkbox
