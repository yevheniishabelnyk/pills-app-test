import { moderateScale } from 'src/utils/scaling'

export default {
  wrapper: {
    width: moderateScale(300),
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'black',
    paddingHorizontal: moderateScale(10),
    paddingVertical: moderateScale(10),
  },

  time: {
    fontSize: moderateScale(16),
    marginRight: moderateScale(10),
  },

  pillName: {
    fontSize: moderateScale(16),
  },

  square: {
    width: moderateScale(20),
    height: moderateScale(20),
    borderWidth: 1,
    borderColor: 'black',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 'auto',
  },

  icon: {
    width: moderateScale(15),
    height: moderateScale(15),
  },
}
