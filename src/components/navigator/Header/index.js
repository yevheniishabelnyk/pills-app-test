import React from 'react'
import PropTypes from 'prop-types'

import { View, TouchableOpacity } from 'react-native'

import Icon from 'src/components/navigator/Icon'

import menuIcon from 'src/assets/menu-icon.png'

import styles from './styles'

const Header = ({ navigation }) => {
  return (
    <TouchableOpacity onPress={() => navigation.toggleDrawer()}>
      <View style={styles.iconWrapper}>
        <Icon icon={menuIcon} style={styles.icon} />
      </View>
    </TouchableOpacity>
  )
}

Header.propTypes = {
  navigation: PropTypes.object,
}

export default Header
