import { moderateScale } from 'src/utils/scaling'

export default {
  iconWrapper: {
    paddingHorizontal: moderateScale(10),
  },

  icon: {
    width: moderateScale(50),
    height: moderateScale(50),
  },
}
