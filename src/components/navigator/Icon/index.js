import React from 'react'
import PropTypes from 'prop-types'

import { Image } from 'react-native'

import styles from './styles'

const Icon = ({ icon, style }) => {
  return <Image source={icon} style={[styles.menuIcon, style]} />
}

Icon.propTypes = {
  icon: PropTypes.number.isRequired,
  style: PropTypes.object,
}

export default Icon
