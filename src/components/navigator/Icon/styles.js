import { moderateScale } from 'src/utils/scaling'

export default {
  menuIcon: {
    width: moderateScale(30),
    height: moderateScale(30),
  },
}
