import React from 'react'

import {
  createSwitchNavigator,
  createStackNavigator,
  createDrawerNavigator,
  createBottomTabNavigator,
} from 'react-navigation'

import DashBoard from 'src/screens/Dashboard'
import Medication from 'src/screens//Medication'
import Calendar from 'src/screens/Calendar'
import MedicalInfo from 'src/screens/MedicalInfo'

import Header from 'src/components/navigator/Header'
import SideMenu from 'src/components/drawer/SideMenu'

import Icon from 'src/components/navigator/Icon'

import homeIcon from 'src/assets/home-icon.png'
import pillIcon from 'src/assets/pill-icon.png'
import settingsIcon from 'src/assets/settings-icon.png'

const MedicalTabStackNavigator = createStackNavigator(
  {
    Medication: {
      screen: Medication,
    },
    MedicalInfo: {
      screen: MedicalInfo,
    },
  },
  {
    initialRouteName: 'Medication',
    headerMode: 'none',
  }
)

const AppTabNavigator = createBottomTabNavigator({
  DashBoard: {
    screen: DashBoard,
    navigationOptions: {
      tabBarLabel: 'DashBoard',
      tabBarIcon: () => <Icon icon={homeIcon} />,
    },
  },

  Medication: {
    screen: MedicalTabStackNavigator,
    navigationOptions: {
      tabBarLabel: 'Medication',
      tabBarIcon: () => <Icon icon={pillIcon} />,
    },
  },

  Calendar: {
    screen: Calendar,
    navigationOptions: {
      tabBarLabel: 'Calendar',
      tabBarIcon: () => <Icon icon={settingsIcon} />,
    },
  },
})

const AppStackNavigator = createStackNavigator({
  AppTabNavigator: {
    screen: AppTabNavigator,
    navigationOptions: ({ navigation }) => ({
      headerLeft: <Header navigation={navigation} />,
    }),
  },
})

AppTabNavigator.navigationOptions = ({ navigation }) => {
  const index = navigation.state.index

  const { routeName } = navigation.state.routes[index]
  const { state } = navigation

  const routes = state.routes[index].routes

  const title = routes
    ? routes[index]
      ? routes[index].params.title
      : null
    : null

  let headerTitle

  if (title) {
    headerTitle = title
  } else {
    headerTitle = routeName
  }

  return {
    headerTitle,
  }
}

const AppDrawerNavigator = createDrawerNavigator(
  {
    Home: AppStackNavigator,
  },
  {
    contentComponent: ({ navigation }) => <SideMenu navigation={navigation} />,
    navigationOptions: {
      drawerLockMode: 'locked-close',
    },
  }
)

const MainNavigator = createSwitchNavigator({
  App: AppDrawerNavigator,
})

export default MainNavigator
