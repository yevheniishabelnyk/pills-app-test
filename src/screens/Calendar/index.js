import React from 'react'
import { Text } from 'react-native'

import Layout from 'src/screens/Layout'

import styles from './styles'

const Calendar = () => {
  return (
    <Layout>
      <Text style={styles.text}>Calendar.</Text>
      <Text style={styles.text}>Nothing here!</Text>
    </Layout>
  )
}
export default Calendar
