import { moderateScale } from 'src/utils/scaling'

export default {
  text: {
    fontSize: moderateScale(26),
  },
}
