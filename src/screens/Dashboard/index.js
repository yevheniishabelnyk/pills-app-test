import React from 'react'

import Layout from 'src/screens/Layout'

import UserInfo from 'src/components/dashboard/UserInfo'
import Messages from 'src/components/dashboard/Messages'

import avatarIcon from 'src/assets/avatar-icon.png'

const DashBoard = () => {
  return (
    <Layout>
      <UserInfo text="Hi, Yevhenii!" avatar={avatarIcon} />

      <Messages title="Messages" />
    </Layout>
  )
}
export default DashBoard
