import { moderateScale } from 'src/utils/scaling'

export default {
  wrapper: {
    flex: 1,
    backgroundColor: 'rgb(249,250,252)',
    paddingHorizontal: moderateScale(20),
  },

  container: {
    flex: 1,
    width: moderateScale(300),
    alignItems: 'center',
    alignSelf: 'center',
    marginTop: moderateScale(50),
  },
}
