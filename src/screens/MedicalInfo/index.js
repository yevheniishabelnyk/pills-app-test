import React from 'react'
import PropTypes from 'prop-types'

import { Text } from 'react-native'

import Layout from 'src/screens/Layout'

import styles from './styles'

const MedicalInfo = ({ navigation }) => {
  const name = navigation.getParam('name')
  const time = navigation.getParam('time')

  return (
    <Layout>
      <Text style={styles.goBackButton} onPress={() => navigation.goBack()}>
        Go back button
      </Text>

      <Text style={styles.name}>{name}</Text>

      <Text style={styles.time}>{time}</Text>
    </Layout>
  )
}

MedicalInfo.propTypes = {
  navigation: PropTypes.object,
}

export default MedicalInfo
