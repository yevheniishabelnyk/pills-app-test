import { moderateScale } from 'src/utils/scaling'

export default {
  goBackButton: {
    fontSize: moderateScale(20),
    color: 'blue',
    marginBottom: moderateScale(50),
  },

  name: {
    fontSize: moderateScale(30),
  },

  time: {
    fontSize: moderateScale(20),
  },
}
