import React, { Component } from 'react'
import { View, Text } from 'react-native'

import styles from './styles'

import Layout from 'src/screens/Layout'
import Checkbox from 'src/components/medication/Checkbox'

class Medication extends Component {
  state = {
    medications: [
      { id: 1, name: 'Analgin', time: '12:03', checked: true },
      { id: 2, name: 'Aspirin', time: '15:00', checked: true },
    ],
  }

  onCheckboxPressed = (name, time) => {
    this.props.navigation.navigate('MedicalInfo', {
      title: 'Medication details',
      name,
      time,
    })
  }

  render() {
    const { medications } = this.state

    return (
      <Layout>
        <Text style={styles.title}>Medication</Text>

        {medications.map((medication, index) => (
          <View style={styles.checkboxWrapper} key={index}>
            <Checkbox
              time={medication.time}
              name={medication.name}
              checked={medication.checked}
              onPress={this.onCheckboxPressed}
            />
          </View>
        ))}
      </Layout>
    )
  }
}
export default Medication
