import { moderateScale } from 'src/utils/scaling'

export default {
  title: {
    fontSize: moderateScale(30),
    marginBottom: moderateScale(20),
  },

  checkboxWrapper: {
    marginBottom: moderateScale(10),
  },
}
