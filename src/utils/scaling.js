import { Dimensions } from 'react-native'

const { width } = Dimensions.get('window')

//Guideline sizes are based on iPhone 6 screen
const guidelineBaseWidth = 375

const defaultScaleFactor = width < guidelineBaseWidth ? 1 : 0.5

export const scale = size => (width / guidelineBaseWidth) * size

export const moderateScale = (size, factor = defaultScaleFactor) =>
  size + (scale(size) - size) * factor
